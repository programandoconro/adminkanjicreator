# Web App to create, manage and study Kanji flash cards.

## Features:

- Firebase Auth.
- Firestore.
- Firebase hosting.
- 100% Functional components.
- Using React-Hooks: `useState`,`useEffect`,`useContext`, `useHistory`.
- Written in Typescript.

Available in: https://kanjiholic.com/ and kanjicreator-5c93c.web.app

**Android App soon also available**
