import { Container, Button } from "@material-ui/core/";
import { Link } from "react-router-dom";
const Home = (handleSignOut: any) => {
  return (
    <Container>
      <h1>Welcome</h1>
      <Link to="/">
        <Button onClick={handleSignOut}> Log Out</Button>
      </Link>
      <Link to="/dashboard">
        <Button> Dash</Button>
      </Link>
    </Container>
  );
};

export default Home;
