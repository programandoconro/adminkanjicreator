import { Container, Button } from "@material-ui/core/";
import { Link } from "react-router-dom";

const Dashboard = () => {
  return (
    <Container>
      <Link to="/">
        <Button> HOME</Button>
      </Link>
      <h1>Dashboard</h1>
    </Container>
  );
};

export default Dashboard;
