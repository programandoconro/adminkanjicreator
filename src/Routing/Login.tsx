import { useState, useContext } from "react";
import {
  Button,
  Card,
  Container,
  Typography,
  Box,
  TextField,
} from "@material-ui/core/";
import { useHistory } from "react-router-dom";
import { signIn } from "../Storage/Auth";
import { UserContext } from "../App";

const Login = () => {
  const [mail, setMail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const { user, setUser } = useContext(UserContext);

  const handleSignIn = async (m: string, p: string) => {
    await signIn(m, p).then((e) => {
      console.log("USERID", e, user);
      setUser(e);
    });
  };

  const history = useHistory();
  const handleEnter = () => {
    history.push("/");
  };

  const handleOnKey = (e: any) => {
    if (e.key === "Enter") {
      e.preventDefault();
      handleSignIn(mail, password);
      handleEnter();
    }
  };
  const handleRegister = () => {
    history.push("/register");
  };

  return (
    <Card className="Card" raised={true}>
      <Container className="Input">
        <Typography className="Sign-in" paragraph={true} variant="inherit">
          Sign in
        </Typography>
        <Box
          className="Box"
          borderColor="error.main"
          border={2}
          borderRadius="borderRadius"
        >
          <Container>
            <TextField
              fullWidth={true}
              placeholder=" email"
              value={mail}
              onChange={(e) => {
                setMail(e.target.value);
              }}
              onKeyDown={(e) => {
                handleOnKey(e);
              }}
            />
          </Container>
        </Box>
      </Container>
      <Container className="Input">
        <Box
          className="Box"
          borderColor="error.main"
          borderRadius="borderRadius"
          border={2}
        >
          <Container>
            <TextField
              fullWidth={true}
              placeholder=" password"
              value={password}
              onChange={(e) => {
                setPassword(e.target.value);
              }}
              type="password"
              onKeyDown={(e) => {
                handleOnKey(e);
              }}
            />
          </Container>
        </Box>
        <h1> </h1>
        <Button
          onClick={() => {
            handleSignIn(mail, password);
          }}
          fullWidth={true}
          color="primary"
          variant="contained"
          type="submit"
        >
          Sign in
        </Button>
        <h1> </h1>
        <Box className="Sign-in">
          <Button onClick={handleRegister} size="small">
            {" "}
            Don't have an account? Register
          </Button>
        </Box>
        <h1> </h1>
      </Container>
    </Card>
  );
};

export default Login;
