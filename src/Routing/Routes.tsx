import { Route, BrowserRouter as Router, Redirect } from "react-router-dom";
import Login from "./Login";
import Register from "./Register";
import { useContext, useState, useEffect } from "react";
import { UserContext } from "../App";
import { signOut } from "../Storage/Auth";
import Home from "./Pages/Home";
import Dashboard from "./Pages/Dashboard";

const Routes = () => {
  const { auth, setAuth, logging } = useContext(UserContext);
  const handleSignOut = () => {
    signOut(setAuth);
    console.log("Auth", auth);
  };
  const pathname = window.location.pathname;
  const [path, setPath] = useState(pathname);

  useEffect(() => {
    path === "/login" && setPath("/");
    path !== "/" &&
      path !== "/dashboard" &&
      path !== "/login" &&
      path !== "/register" &&
      setPath("/");
  }, [path]);

  return (
    <Router>
      <Route
        path="/"
        render={() =>
          logging ? <Redirect to={"/login"} /> : <Redirect to={path} />
        }
      />
      <Route exact path="/" render={() => auth && Home(handleSignOut)} />
      <Route exact path="/dashboard" render={() => auth && <Dashboard />} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/register" component={Register} />
    </Router>
  );
};

export { Routes };
