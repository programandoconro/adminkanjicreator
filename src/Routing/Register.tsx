import { useState } from "react";
import {
  Button,
  Card,
  Container,
  Typography,
  Box,
  TextField,
} from "@material-ui/core/";
import { useHistory } from "react-router-dom";
import { register } from "../Storage/Auth";

const Register = () => {
  const [mail, setMail] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const handleRegister = (m: string, p: string) => {
    register(m, p);
  };

  const history = useHistory();
  const handleEnter = () => {
    history.push("/");
  };

  const handleOnKey = (e: any) => {
    if (e.key === "Enter") {
      e.preventDefault();
      handleEnter();
    }
  };
  const handleLogin = () => {
    history.push("/login");
  };

  return (
    <Card className="Card" raised={true}>
      <Container className="Input">
        <Typography className="Sign-in" paragraph={true} variant="inherit">
          Register
        </Typography>
        <Box
          className="Box"
          borderColor="error.main"
          border={2}
          borderRadius="borderRadius"
        >
          <Container>
            <TextField
              fullWidth={true}
              placeholder=" email"
              value={mail}
              onChange={(e) => {
                setMail(e.target.value);
              }}
              onKeyDown={(e) => {
                handleOnKey(e);
              }}
            />
          </Container>
        </Box>
      </Container>
      <Container className="Input">
        <Box
          className="Box"
          borderColor="error.main"
          borderRadius="borderRadius"
          border={2}
        >
          <Container>
            <TextField
              fullWidth={true}
              placeholder=" password"
              value={password}
              onChange={(e) => {
                setPassword(e.target.value);
              }}
              type="password"
              onKeyDown={(e) => {
                handleOnKey(e);
              }}
            />
          </Container>
        </Box>
        <h1> </h1>
        <Button
          fullWidth={true}
          color="primary"
          variant="contained"
          type="submit"
          onClick={() => {
            handleRegister(mail, password);
          }}
        >
          Register
        </Button>
        <h1> </h1>
        <h1> </h1>
        <Box className="Sign-in">
          <Button onClick={handleLogin} size="small">
            {" "}
            Already have an account? Sign in{" "}
          </Button>
        </Box>
      </Container>
    </Card>
  );
};

export default Register;
