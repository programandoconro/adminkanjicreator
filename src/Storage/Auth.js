import firebase from "firebase/app";
import "firebase/auth";

const auth = () => firebase.auth();

const signIn = async (email, password) => {
  await auth()
    .signInWithEmailAndPassword(email, password)
    .then((userCredential) => {
      var user = userCredential.user;
      console.log("USER", user);
      return user.uid;
    })
    .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
      alert(errorCode, errorMessage);
      return null;
    });
};

const register = (email, password) => {
  auth()
    .createUserWithEmailAndPassword(email, password)
    .then((userCredential) => {
      var user = userCredential.user;
      console.log(user);
    })
    .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
      alert(errorCode, errorMessage);
    });
};

const onAuthChange = (setState, setLoading) => {
  auth().onAuthStateChanged((u) => {
    if (!u) {
      console.log(u);
      setLoading(true);
    } else {
      setState(u);
      setLoading(false);
    }
  });
};

const signOut = (setState) => {
  auth()
    .signOut()
    .then(function () {
      console.log("LOGGED OUT");
    })
    .catch(function (error) {
      console.log("ERROR LOGGING OUT");
    });
  setState(null);
};
export { signIn, register, signOut, onAuthChange };
