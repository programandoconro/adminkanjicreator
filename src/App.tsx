import { useEffect } from "react";
import { Routes } from "./Routing/Routes";
import "./App.css";
import { Container } from "@material-ui/core/";
import initFirebase from "./Storage/Secret";
import { useState, createContext } from "react";
import { onAuthChange } from "./Storage/Auth";

export const UserContext = createContext<any>(null);
function App() {
  const [user, setUser] = useState(null);
  const [auth, setAuth] = useState<string | null>(null);
  const [logging, setLogging] = useState<boolean | null> (null)
  useEffect(() => {
    initFirebase();
  }, []);

  useEffect(() => {
    onAuthChange(setAuth,setLogging);
  }, [auth]);

  return (
    <UserContext.Provider value={{ user, setUser, auth,setAuth,logging }}>
      <div className="App">
        <Container>
          <Routes />
        </Container>
      </div>
    </UserContext.Provider>
  );
}

export default App;
